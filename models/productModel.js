module.exports = (sequelize, DataTypes) => {

    const Product = sequelize.define("product", {
        title: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique:true,
            validate: {
                len: [3, 30],
                notNull: { msg: 'Please enter your title' }
              }
        },
        price: {
            type: DataTypes.INTEGER,
            allowNull:false,
            validate: {
                notNull: { msg: 'Please enter  price' }
              }
        },
        description: {
            type: DataTypes.TEXT,
            allowNull:false,
            validate: {
                notNull: { msg: 'Please enter  description' }
              }
        },
        published: {
            type: DataTypes.BOOLEAN
        },
    
    });
    // Product.addHook('beforeValidate', (product, options) => {
    //     console.log(" beforeValidate:");

    //     product.title = 'add';
    //     console.log(product.title);
    //   });
    Product.afterCreate(async (product, options) => {
        console.log("New product added:");
        console.log(`this is new item ${product.title} added`);
        console.log(`price of product is: ${product.price}`);
      });
      Product.beforeCreate(async ( product) => {
        if (product.price > 10000 ) {
          throw new Error('product price must be less than 10000');
        }
      });

    return Product

}