const db = require('../models')

const Product = db.products
const { Sequelize, QueryTypes, Transaction}= require('sequelize')
const addProduct = async (req, res) => {
try{
    const title = req.body.title
    const productExists = await Product.findOne({where: {title:title}})
    if(productExists){
        return res.status(400).json({message:'product already exist'})
    }
    let info = {
        title: req.body.title,
        price: req.body.price,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
    }
    const product = await Product.create(info)
    return res.status(200).json(product)
}catch(e){
    return res.json({message:e.message})
}

}
// get all products
const getAllProducts = async (req, res) => {
    try{
    let products = await Product.findAll({
        // order: ['price', 'DESC']
    })
    return res.status(200).json(products)
}catch(e){
    return res.json({message:e.message})
}
}

// get single product
const getOneProduct = async (req, res) => {
    try{
    let id = req.params.id
    let product = await Product.findOne({ where: { id: id }})
    return res.status(200).json(product)
}catch(e){
    return res.json({message:e.message})
}
}

//  update Product
const updateProduct = async (req, res) => {
    try{
    let id = req.params.id
    const product = await Product.update(req.body, { where: { id: id }})
    return res.status(200).json(product)
}catch(e){
    return res.json({message:e.message})
}
}

//delete product by id\
const deleteProduct = async (req, res) => {
    try{ 
    let id = req.params.id  
    await Product.destroy({ where: { id: id }} )
    return res.status(200).json({message:'Product is deleted !'})
}catch(e){
    return res.json({message:e.message})
}

}
//  get published product

const getPublishedProduct = async (req, res) => {
    try{ 
    const products =  await Product.findAll({ where: { published: true }})
    return res.status(200).json(products)
}catch(e){
    return res.json({message:e.message})
}
}
const groupProduct = async (req, res) => {
        try{
        let products = await Product.findAll({ })
        return res.status(200).json(products)
    }catch(e){
        return res.json({message:e.message})
    }
}
// const { Transaction } = require('sequelize')
// const Transaction_sequelize = async(req, res) => {
//     let  t  = await db.sequelize.transaction()
//     try {
//         const product1 = await Product.create({
//         title: req.body.title,
//         price: req.body.price,
//         description: req.body.description,
//         published: req.body.published ? req.body.published : false
//       }, { transaction: t });
//       await t.commit();
//       return res.json( `product added${product1}` )
//     } catch (error) {
//      await t.rollback();
//      console.log('rollback');
//      return res.json({message:error.message})
//     }
// }

const rawQuery = async(req,res)=> {
        // const Products = await db.sequelize.query("SELECT * FROM Products", { type: QueryTypes.SELECT });
        
         const Products = await db.sequelize.query(
            'SELECT * FROM Products WHERE title LIKE :search_name',
            {
              replacements: { search_name: 'b%' },
              type: QueryTypes.SELECT
            });        
        
   return res.status(200).json(Products)
}


const Transaction_sequelize = async(req, res) => {
    let t  = await db.sequelize.transaction({
        isolationLevel:Transaction.ISOLATION_LEVELS.SERIALIZABLE
    })
    try {
        const product1 = await Product.create({
        title: req.body.title,
        price: req.body.price,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
      }, { transaction: t });
      await t.commit();
      return res.json( product1 )
    } catch (error) {
     await t.rollback();
     console.log('rollback');
     return res.json({message:error.message})
    }
}
// =======================   READ_UNCOMMITTED    ===================

// const one = async (req, res) => {
//     let t1 = await db.sequelize.transaction({
//         isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED
//     })
//     try {
//         await Product.findAll({ where: { id: 1 }, lock: true });
//         let products =await Product.update({ price:  1234 }, { where: { id: 1 }, transaction: t1 });
//         setTimeout(async() => {
//             try {
//                 await t1.commit();
//                 return res.json(products);
//             } catch (error) {
//                 await t1.rollback();
//             }
//         }, 10000);
//     } catch (error) {
//         console.log(error);
//         await t1.rollback();
//         return res.json(error);
//     }
// };

// const two = async(req, res) => {
//     let t2 = await db.sequelize.transaction({
//         isolationLevel: Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED
//     })
//     try {
//         setTimeout(async() => {
//             let products = await Product.findAll({ where: { id: 1 }, transaction: t2 });
//             await t2.commit();
//             return res.json(products);
//         }, 1000);
//     } catch (error) {
//         console.log(error);
//         await t2.rollback();
//         return res.json(error);
//     }
// };
// =======================   READ_COMMITTED ===================
// const one = async (req, res) => {
//     let t1 = await db.sequelize.transaction({
//         isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
//     })
//     try {
//         await Product.findAll({ where: { id: 1 }, lock: true });
//         let products =await Product.update({ price:  1000 }, { where: { id: 1 }, transaction: t1 });
//         setTimeout(async() => {
//             try {
//                 await t1.commit();
//                 return res.json(products);
//             } catch (error) {
//                 await t1.rollback();
//             }
//         }, 10000);
//     } catch (error) {
//         console.log(error);
//         await t1.rollback();
//         return res.json(error);
//     }
// };

// const two = async(req, res) => {
//     let t2 = await db.sequelize.transaction({
//         isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
//     })
//     try {
//         setTimeout(async() => {
//             let products = await Product.findAll({ where: { id: 1 }, transaction: t2 });
//             await t2.commit();
//             return res.json(products);
//         }, 1000);
//     } catch (error) {
//         console.log(error);
//         await t2.rollback();
//         return res.json(error);
//     }
// };


// =======================   REPEATABLE_READ     ===================
// const one = async (req, res) => {
//     let t1 = await db.sequelize.transaction({
//         isolationLevel: Transaction.ISOLATION_LEVELS.REPEATABLE_READ
//     })
//     try {
//         await Product.findAll({ where: { id: 1 }, lock: true });
//         let products =await Product.update({ price:  1234 }, { where: { id: 1 }, transaction: t1 });
//         setTimeout(async() => {
//             try {
//                 await t1.commit();
//                 return res.json(products);
//             } catch (error) {
//                 await t1.rollback();
//             }
//         }, 6000);
//     } catch (error) {
//         console.log(error);
//         await t1.rollback();
//         return res.json(error);
//     }
// };

// const two = async(req, res) => {
//     let t2 = await db.sequelize.transaction({
//         isolationLevel: Transaction.ISOLATION_LEVELS.REPEATABLE_READ
//     })
//     try {
//         setTimeout(async() => {
//             let products = await Product.findAll({ where: { id: 1 }, transaction: t2 });
//             await t2.commit();
//             return res.json(products);
//         }, 1000);
//     } catch (error) {
//         console.log(error);
//         await t2.rollback();
//         return res.json(error);
//     }
// };


// =======================   SERIALIZABLE ===================
const one = async (req, res) => {
    let t1 = await db.sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
    })
    try {
        await Product.findAll({ where: { id: 1 }, lock: true });
        let products =await Product.update({ price:  1000 }, { where: { id: 1 }, transaction: t1 });
        setTimeout(async() => {
            try {
                await t1.commit();
                return res.json(products);
            } catch (error) {
                await t1.rollback();
            }
        }, 10000);
    } catch (error) {
        console.log(error);
        await t1.rollback();
        return res.json(error);
    }
};

const two = async(req, res) => {
    let t2 = await db.sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
    })
    try {
        setTimeout(async() => {
            let products = await Product.findAll({ where: { id: 1 }, transaction: t2 });
            await t2.commit();
            return res.json(products);
        }, 1000);
    } catch (error) {
        console.log(error);
        await t2.rollback();
        return res.json(error);
    }
};

module.exports = {
    addProduct,
    getAllProducts,
    getOneProduct,
    updateProduct,
    deleteProduct,
    getPublishedProduct,
    Transaction_sequelize,
    groupProduct,
    rawQuery,
    one,
    two
}