const productController = require('../controllers/productController.js')

const router = require('express').Router()


router.post('/addProduct',productController.addProduct)

router.get('/allProducts', productController.getAllProducts)

router.get('/published', productController.getPublishedProduct)

router.get('/one/:id', productController.getOneProduct)

router.put('/:id', productController.updateProduct)

router.delete('/:id', productController.deleteProduct)

router.post('/Transaction', productController.Transaction_sequelize)

router.get('/rawQuery', productController.rawQuery)

router.post('/one', productController.one)

router.get('/two', productController.two)


module.exports = router